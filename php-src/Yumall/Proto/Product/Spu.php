<?php
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: proto/product.proto

namespace Yumall\Proto\Product;

use Google\Protobuf\Internal\GPBType;
use Google\Protobuf\Internal\RepeatedField;
use Google\Protobuf\Internal\GPBUtil;

/**
 * Generated from protobuf message <code>yumall.proto.product.Spu</code>
 */
class Spu extends \Google\Protobuf\Internal\Message
{
    /**
     * Generated from protobuf field <code>int64 id = 1;</code>
     */
    private $id = 0;
    /**
     * Generated from protobuf field <code>string code = 2;</code>
     */
    private $code = '';
    /**
     * Generated from protobuf field <code>.yumall.proto.product.Category category = 3;</code>
     */
    private $category = null;
    /**
     * Generated from protobuf field <code>repeated .yumall.proto.product.Sku skuList = 4;</code>
     */
    private $skuList;

    /**
     * Constructor.
     *
     * @param array $data {
     *     Optional. Data for populating the Message object.
     *
     *     @type int|string $id
     *     @type string $code
     *     @type \Yumall\Proto\Product\Category $category
     *     @type \Yumall\Proto\Product\Sku[]|\Google\Protobuf\Internal\RepeatedField $skuList
     * }
     */
    public function __construct($data = NULL) {
        \GPBMetadata\Proto\Product::initOnce();
        parent::__construct($data);
    }

    /**
     * Generated from protobuf field <code>int64 id = 1;</code>
     * @return int|string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Generated from protobuf field <code>int64 id = 1;</code>
     * @param int|string $var
     * @return $this
     */
    public function setId($var)
    {
        GPBUtil::checkInt64($var);
        $this->id = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string code = 2;</code>
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Generated from protobuf field <code>string code = 2;</code>
     * @param string $var
     * @return $this
     */
    public function setCode($var)
    {
        GPBUtil::checkString($var, True);
        $this->code = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>.yumall.proto.product.Category category = 3;</code>
     * @return \Yumall\Proto\Product\Category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Generated from protobuf field <code>.yumall.proto.product.Category category = 3;</code>
     * @param \Yumall\Proto\Product\Category $var
     * @return $this
     */
    public function setCategory($var)
    {
        GPBUtil::checkMessage($var, \Yumall\Proto\Product\Category::class);
        $this->category = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>repeated .yumall.proto.product.Sku skuList = 4;</code>
     * @return \Google\Protobuf\Internal\RepeatedField
     */
    public function getSkuList()
    {
        return $this->skuList;
    }

    /**
     * Generated from protobuf field <code>repeated .yumall.proto.product.Sku skuList = 4;</code>
     * @param \Yumall\Proto\Product\Sku[]|\Google\Protobuf\Internal\RepeatedField $var
     * @return $this
     */
    public function setSkuList($var)
    {
        $arr = GPBUtil::checkRepeatedField($var, \Google\Protobuf\Internal\GPBType::MESSAGE, \Yumall\Proto\Product\Sku::class);
        $this->skuList = $arr;

        return $this;
    }

}

