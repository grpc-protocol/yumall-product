#!/bin/bash

rm -rf go-src/* && \
protoc ./proto/*.proto --go_out=plugins=grpc:go-src && \
rm -rf php-src/* && \
protoc ./proto/*.proto --php_out=php-src 