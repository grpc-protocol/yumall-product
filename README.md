# Products proto

go build:
```
protoc ./proto/*.proto --go_out=plugins=grpc:go-src
```

php build:
```
protoc ./proto/*.proto --php_out=php-src 
```